/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package com.mulesoft.connectors.zillowconnector.internal.util;

public final class Constants {
    //constants used in operations

    /*
    public static final String DOC_TYPE = "docType";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String APPLICATION_JSON = "application/json";
    public static final String UTF_8 = "UTF-8";
    */

    public static final String zws_id = "zws_id";
    public static final String state = "state";
    public static final String city = "city";
    public static final String childtype = "childtype";
    public static final String test_zws_id = "X1-ZWz1h3cu8czs3v_73nhl";
    public static final String test_state = "wa";
    public static final String test_city = "seattle";
    public static final String test_childtype = "neighborhood";

}
