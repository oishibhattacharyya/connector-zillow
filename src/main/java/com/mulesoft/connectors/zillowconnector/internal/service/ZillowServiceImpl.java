/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package com.mulesoft.connectors.zillowconnector.internal.service;

import com.mulesoft.connectors.zillowconnector.api.*;
import com.mulesoft.connectors.zillowconnector.api.ResponseStatus;
import com.mulesoft.connectors.zillowconnector.internal.config.ZillowConfiguration;
import com.mulesoft.connectors.zillowconnector.internal.connection.ZillowConnection;
import com.mulesoft.connectors.zillowconnector.internal.util.ZillowUtil;
import com.mulesoft.connectors.zillowconnector.internal.util.Urls;
import com.mulesoft.connectors.zillowconnector.internal.util.RequestService;
import org.mule.connectors.commons.template.service.DefaultConnectorService;
import org.mule.runtime.extension.api.runtime.operation.Result;
import org.mule.runtime.http.api.HttpConstants;
import org.mule.runtime.http.api.domain.message.request.HttpRequest;
import org.mule.runtime.http.api.domain.message.response.HttpResponse;

import java.io.InputStream;

import static com.mulesoft.connectors.zillowconnector.internal.attributes.AttributesUtil.setResponseAttributesForSend;
import static com.mulesoft.connectors.zillowconnector.internal.util.ClassForName.GET_REGION_CHILDREN_DTO;
import static com.mulesoft.connectors.zillowconnector.internal.util.Constants.*;
import static com.mulesoft.connectors.zillowconnector.internal.util.Constants.childtype;
//import static com.mulesoft.connectors.zillowconnector.internal.exception.ExceptionHandler.checkError;
//import static com.mulesoft.connectors.zillowconnector.internal.exception.ExceptionHandler.checkErrorAsync;


public class ZillowServiceImpl extends DefaultConnectorService<ZillowConfiguration, ZillowConnection> implements ZillowService {
    public ZillowServiceImpl(ZillowConfiguration config, ZillowConnection connection) {
        super(config, connection);
    }

    public Result<GetRegionChildrenDTO, ResponseStatus> getResultOfRegionChildren() {
        String strUri = getConfig().getAddress() +  Urls.Get_Region_Children;
        HttpRequest request = getConnection().getHttpRequestBuilder().method(HttpConstants.Method.GET).uri(strUri)
                .addQueryParam(state, test_state)
                .addQueryParam(city, test_city)
                .addQueryParam(childtype, test_childtype)
                .addQueryParam(zws_id, test_zws_id)
                .build();
        HttpResponse httpResponse = RequestService.requestCall(request, false, getConnection());
        InputStream response = httpResponse.getEntity().getContent();
        //checkError(httpResponse);
        Object dto1 = ZillowUtil.getDtoObject(response, GET_REGION_CHILDREN_DTO);
        GetRegionChildrenDTO dto = GetRegionChildrenDTO.class.cast(dto1);
        return Result.<GetRegionChildrenDTO, ResponseStatus>builder().output(dto).attributes(setResponseAttributesForSend(httpResponse)).build();
    }
}