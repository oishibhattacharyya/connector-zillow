/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package com.mulesoft.connectors.zillowconnector.internal.operations;

import com.mulesoft.connectors.zillowconnector.api.*;
import com.mulesoft.connectors.zillowconnector.api.ResponseStatus;
import com.mulesoft.connectors.zillowconnector.internal.config.ZillowConfiguration;
import com.mulesoft.connectors.zillowconnector.internal.connection.ZillowConnection;
//import com.mulesoft.connectors.zillowconnector.internal.error.ErrorProvider;
import com.mulesoft.connectors.zillowconnector.internal.service.ZillowService;
import com.mulesoft.connectors.zillowconnector.internal.service.ZillowServiceImpl;
import org.mule.connectors.commons.template.operation.ConnectorOperations;
import org.mule.runtime.extension.api.annotation.param.*;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.runtime.operation.Result;

import static org.mule.runtime.extension.api.annotation.param.MediaType.*;

public class ZillowOperations extends ConnectorOperations<ZillowConfiguration, ZillowConnection, ZillowService> {

    public ZillowOperations() {
        super(ZillowServiceImpl::new);
    }

    /**
     * Method to get a list of the available domains for a given bilingual language dataset.
     *
     * @param configuration Configuration Object
     * @param connection    Connection object.
     * @return status of add request in json format
     */
    @DisplayName(value = "Get Region Children")
    //@Throws(ErrorProvider.class)
    @MediaType(value = ANY, strict = false)
    public Result<GetRegionChildrenDTO, ResponseStatus> getResultOfRegionChildren(@Config ZillowConfiguration configuration, @Connection ZillowConnection connection) {
        return newExecutionBuilder(configuration, connection)
                .execute(ZillowService::getResultOfRegionChildren);
    }
}
