/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package com.mulesoft.connectors.zillowconnector.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder({
        "region",
        "subregiontype",
        "list"
})

public class Response {

    @JsonProperty("region")
    private String region;

    @JsonProperty("subregiontype")
    private String subregiontype;

    @JsonProperty("regionList")
    private RegionList regionList;

    @JsonProperty("region")
    public String getRegion() {
        return region;
    }

    @JsonProperty("region")
    public void setRegion(String region) {
        this.region = region;
    }

    @JsonProperty("subregiontype")
    public String getSubRegionType() {
        return subregiontype;
    }

    @JsonProperty("subregiontype")
    public void setSubRegionType(String subregiontype) {
        this.subregiontype = subregiontype;
    }

    @JsonProperty("regionList")
    public RegionList getRegionList() {
        return regionList;
    }

    @JsonProperty("regionList")
    public void setRegionList(RegionList regionList) {
        this.regionList = regionList;
    }
}