/**
 * (c) 2003-2019 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package com.mulesoft.connectors.zillowconnector.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;


@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder({
        "count",
        "region"
})

public class RegionList {

    @JsonProperty("count")
    private String count;

    @JsonProperty("region")
    private List<Region> region;

    @JsonProperty("count")
    public String getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(String state) {
        this.count = count;
    }

    @JsonProperty("region")
    public List<Region> getRegion() {
        return region;
    }

    @JsonProperty("region")
    public void setRegion(List<Region> region) {
        this.region = region;
    }
}